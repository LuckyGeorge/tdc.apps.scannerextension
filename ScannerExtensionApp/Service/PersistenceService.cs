﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Documents;
using Newtonsoft.Json;
using NLog;
using ScannerExtensionApp.Dto;
using ScannerExtensionApp.Interface;

namespace ScannerExtensionApp.Service
{
    /// <summary>
    /// Service for storing and retrieving scanId / processId pairs. The pairs are stored in a JSON file in the common application data folder
    /// </summary>
    public class PersistenceService : IPersistenceService
    {
        /// <summary>
        /// Stores the ProcessId - ScanId pairs
        /// </summary>
        private List<ScanToProcessDto> _processes = new List<ScanToProcessDto>();

        /// <summary>
        /// Stores the ProcessId's from the calling app
        /// </summary>
        private List<string> _processIdCache; //Should not be here but make a service only for a string list is not really smart

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public string DatabasePath { get; }
        public string DatabaseName { get; }

        /// <summary>
        /// Constructor - Sets the Paths to the data file
        /// </summary>
        public PersistenceService()
        {
            var commonFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

            //Set Path
            DatabasePath = Path.Combine(commonFolderPath, "ScannerExtension");
            DatabaseName = "ScannerProcessData.json";

            _logger.Info($"Starting {nameof(PersistenceService)}  with data file {Path.Combine(DatabasePath, DatabaseName)}");
        }

        /// <summary>
        /// Sets the processIds from the calling app
        /// </summary>
        /// <param name="processIds">List of processIds</param>
        public void SetProcessIds(List<string> processIds)
        {
            _processIdCache = processIds;
        }

        /// <summary>
        /// Gets the stored processIds - these Ids can be displayed in the main window if no processId / scanId pair is found
        /// </summary>
        /// <returns></returns>
        public List<string> GetProcessIds()
        {
            return _processIdCache;
        }

        /// <summary>
        /// Initializes the service and creates the data directory
        /// </summary>
        /// <param name="deleteExistingDatabase">If <b>True</b>, a backup is made and the data file will be deleted. Should only be true for testing purposes</param>
        /// <returns></returns>
        public async Task<bool> Initialize(bool deleteExistingDatabase = false)
        {
            try
            {
                var filePath = Path.Combine(DatabasePath, DatabaseName);
                var backupFilePath = Path.Combine(DatabasePath, DatabaseName + ".bak");

                

                if (!Directory.Exists(DatabasePath))
                     Directory.CreateDirectory(DatabasePath);
                if (!File.Exists(filePath)) return await Task.FromResult(true);

                if (deleteExistingDatabase)
                {
                    if(File.Exists(backupFilePath))
                        File.Delete(backupFilePath);
                    File.Copy(filePath, backupFilePath);
                    File.Delete(filePath);
                    _processes.Clear();
                }
                else
                    return await Reload();
                return await Task.FromResult(true);
            }
            catch (Exception e)
            {
                _logger.Error(e,"Initialize Failed");
            }
            return await Task.FromResult(false);
        }

        /// <summary>
        /// Gets the list of scanId / processId pairs
        /// </summary>
        /// <returns></returns>
        public async Task<List<ScanToProcessDto>> GetProcesses()
        {
            await Initialize(); //Always call initialize - it does not hurt as long as we don't set deleteExistingDatabase = true

            var returnList = new List<ScanToProcessDto>();
            if (_processes != null && _processes.Any())
            {
                returnList.AddRange(_processes);
            }

            if (_processIdCache == null || !_processIdCache.Any()) return returnList;

            foreach (var process in _processIdCache.Where(process => !returnList.Any(p=>p.ProcessId.Equals(process))))
            {
                returnList.Add(new ScanToProcessDto{ProcessId = process});
            }
            return returnList;
        }

        /// <summary>
        /// Adds a new scanId / processId pair if the scanId is not used before
        /// </summary>
        /// <param name="scanId">ScanId of the pair</param>
        /// <param name="processId">ProcessId of the pair</param>
        /// <returns><b>False</b>, if the scanId is already used or the saving of the file fails</returns>
        public async Task<bool> AddProcess(string scanId, string processId)
        {
            await Initialize(); //Always call initialize - it does not hurt as long as we don't set deleteExistingDatabase = true
            if (await HasProcess(scanId)) return false;
            _processes.Add(new ScanToProcessDto {ScanId = scanId, ProcessId = processId});
            return await Persist();
        }

        /// <summary>
        /// Changes a scanId / processId pair if the scanId is used before
        /// </summary>
        /// <param name="oldScanId">Old ScanId of the pair</param>
        /// <param name="newScanId">New ProcessId of the pair</param>
        /// <returns><b>False</b>, if the scanId is already used or the saving of the file fails</returns>
        public async Task<bool> ChangeProcess(string oldScanId, string newScanId)
        {
            await Initialize(); //Always call initialize - it does not hurt as long as we don't set deleteExistingDatabase = true
            if (!await HasProcess(oldScanId)) return false;
            var process = _processes.First(p=>p.ScanId.Equals(oldScanId));
            process.ScanId = newScanId;
            return await Persist();
        }

        /// <summary>
        /// Removes a scanId / processId pair if the scanId is used
        /// </summary>
        /// <param name="scanId">ScanId of the pair</param>
        /// <returns><b>False</b>, if the scanId is not used or the saving of the file fails</returns>
        public async Task<bool> RemoveProcess(string scanId)
        {
            await Initialize(); //Always call initialize - it does not hurt as long as we don't set deleteExistingDatabase = true
            if (!await HasProcess(scanId)) return false;
            var processObject = _processes.First(p => p.ScanId.Equals(scanId));
            _processes.Remove(processObject);
            return await Persist();
        }

        /// <summary>
        /// Gets the processId for the given scanId
        /// </summary>
        /// <param name="scanId">ScanId of the pair</param>
        /// <returns>ProcessId of the pair or <see cref="String.Empty"/> if there is no pair</returns>
        public async Task<string> GetProcess(string scanId)
        {
            await Initialize(); //Always call initialize - it does not hurt as long as we don't set deleteExistingDatabase = true
            return await HasProcess(scanId) ? _processes.First(p => p.ScanId.Equals(scanId)).ProcessId : String.Empty;
        }

        private async Task<bool> HasProcess(string scanId)
        {
            await Initialize(); //Always call initialize - it does not hurt as long as we don't set deleteExistingDatabase = true
            return _processes != null && _processes.Any(p => p.ScanId.Equals(scanId));
        }

        private async Task<bool> Persist()
        {
            if (_processes == null || !_processes.Any()) return false;
            try
            {
                var jsonString = JsonConvert.SerializeObject(_processes, Formatting.Indented);
                using (var outputFile = new StreamWriter(Path.Combine(DatabasePath, DatabaseName)))
                {
                    await outputFile.WriteAsync(jsonString).ConfigureAwait(continueOnCapturedContext: false);
                }
                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Persist Failed");
            }
            return false;
        }

        private async Task<bool> Reload()
        {
            if (!File.Exists(Path.Combine(DatabasePath, DatabaseName))) return false;
            try
            {
                using (var inputFile = new StreamReader(Path.Combine(DatabasePath, DatabaseName)))
                {
                    var jsonString = await inputFile.ReadToEndAsync().ConfigureAwait(continueOnCapturedContext: false);
                    _processes = JsonConvert.DeserializeObject<List<ScanToProcessDto>>(jsonString);
                }

                return true;
            }
            catch (Exception e)
            {
                _logger.Error(e, "Reload Failed");
            }
            return false;
        }
    }
}