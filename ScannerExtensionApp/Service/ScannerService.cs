﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.PointOfService;
using NLog;
using ScannerExtensionApp.Interface;
using Logger = NLog.Logger;

namespace ScannerExtensionApp.Service
{
    public class ScannerService: IScannerService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IPersistenceService _persistenceService;
        private Scanner _scanner;

        public ScannerService(IPersistenceService persistenceService)
        {
            _persistenceService = persistenceService;
        }

        public bool Initialize()
        {
            IsReady = FindScanner();
            return IsReady;
        }

        private bool FindScanner()
        {
            try
            {
                var explorer = new PosExplorer();
                var devices = explorer.GetDevices("Scanner");
                if (devices != null && devices.Count > 0)
                {
                    _logger.Debug($"Scanner found {devices.Count}!");
                    _scanner = explorer.CreateInstance(devices[0]) as Scanner;
                    if (_scanner != null)
                    {
                        _scanner.Open();
                        _scanner.Claim(1000);
                        if (_scanner.Claimed)
                        {
                            _scanner.DeviceEnabled = true;
                            _scanner.DataEventEnabled = true;
                            _scanner.DecodeData = true;
                            _scanner.DataEvent += OnScannerDataEvent;
                            _scanner.ErrorEvent += OnScannerErrorEvent;
                            return true;

                        }

                        _logger.Error($"Could not claim scanner {_scanner.DeviceName}!");
                    }

                    _logger.Error($"Could not create instance of scanner {devices[0].Description}!");
                }
                _logger.Error($"No Scanner found!");
                
            }
            catch (Exception e)
            {
                _logger.Error(e, "FindScanner Failed!");
                Error?.Invoke("");
            }
            return false;
        }

        private void OnScannerErrorEvent(object sender, DeviceErrorEventArgs e)
        {
            Error?.Invoke(e.ErrorCode.ToString());
            _logger.Error($"Scanner error: {e.ErrorCode}!");
        }

        private void OnScannerDataEvent(object sender, DataEventArgs e)
        {
            _logger.Debug($"Scanner data: {e}!");
            ASCIIEncoding encoder = new ASCIIEncoding();
            try
            {
                var scanData = encoder.GetString(_scanner.ScanDataLabel);

                Task.Run(async () =>
                {
                    if (await _persistenceService.GetProcess(scanData) is string processId)
                    {
                        if (!String.IsNullOrEmpty(processId))
                        {
                            _logger.Debug($"ProcessId {processId} for scanId {scanData} found! Sending Event!");
                            ScanAndEventSuccess?.Invoke(scanData, processId);
                        }
                        else
                        {
                            _logger.Debug($"ProcessId for scanId {scanData} NOT found!");
                            ScanNeedsConfirm?.Invoke(scanData);
                        }

                    }
                    _scanner.DataEventEnabled = true;
                });
            }
            catch (PosControlException ex)
            {
                _logger.Error(ex, "ScannerDataEvent Error!");
            }
        }

        public Action<string, string> ScanAndEventSuccess { get; set; }

        public Action<string> ScanNeedsConfirm { get; set; }

        public Action<string> Error { get; set; }

        public Action<string> Init { get; set; }

        public bool IsReady { get; private set; } = false;

        public void Exit()
        {
            if (_scanner == null) return;

            _scanner.DataEvent -= OnScannerDataEvent;
            _scanner.ErrorEvent -= OnScannerErrorEvent;
            try
            {
                // Close the active scanner
                _scanner.Close();
            }
            catch (PosControlException e)
            {
                _logger.Error(e, "Dispose Scanner Failed!");
            }
            finally
            {
                _scanner = null;
            }
        }
    }
}