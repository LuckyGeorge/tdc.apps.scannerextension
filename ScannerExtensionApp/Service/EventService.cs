﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Pipes;
using System.Threading.Tasks;
using NLog;
using ScannerExtensionApp.Interface;

namespace ScannerExtensionApp.Service
{
    public class EventService : IEventService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public const string PipeName = "ScannerExtensionPipe";
        private const int PipeTimeOut = 250;

        /// <summary>
        /// Maybe we can use this method sometimes - currently its only a stub
        /// </summary>
        /// <returns></returns>
        public bool Initialize()
        {
            return true;
        }

        public async Task<bool> SendProcessEvent(string processId)
        {
            if (String.IsNullOrEmpty(processId)) return false;
            try
            {
                using (NamedPipeClientStream pipeClient =
                    new NamedPipeClientStream(".", PipeName, PipeDirection.Out))
                {
                    var byteArray = System.Text.Encoding.ASCII.GetBytes(processId);
                    await pipeClient.ConnectAsync(100);
                    await pipeClient.WriteAsync(byteArray, 0, byteArray.Length);
                    pipeClient.WaitForPipeDrain();

                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "SendProcessEvent Failed");
            }

            return false;
        }
    }
}