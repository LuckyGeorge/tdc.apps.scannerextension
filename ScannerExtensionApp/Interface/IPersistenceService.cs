﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ScannerExtensionApp.Dto;

namespace ScannerExtensionApp.Interface
{
    public interface IPersistenceService
    {
        void SetProcessIds(List<string> processIds);
        List<string> GetProcessIds();
        Task<bool> Initialize(bool deleteExistingDatabase = false);
        Task<List<ScanToProcessDto>> GetProcesses();
        Task<bool> AddProcess(string scanId, string processId);
        Task<bool> RemoveProcess(string scanId);
        Task<string> GetProcess(string scanId);

        /// <summary>
        /// Changes a scanId / processId pair if the scanId is used before
        /// </summary>
        /// <param name="oldScanId">Old ScanId of the pair</param>
        /// <param name="newScanId">New ProcessId of the pair</param>
        /// <returns><b>False</b>, if the scanId is already used or the saving of the file fails</returns>
        Task<bool> ChangeProcess(string oldScanId, string newScanId);
    }
}