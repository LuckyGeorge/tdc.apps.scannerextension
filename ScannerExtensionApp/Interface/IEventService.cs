﻿using System.Threading.Tasks;

namespace ScannerExtensionApp.Interface
{
    public interface IEventService
    {
        bool Initialize();
        Task<bool> SendProcessEvent(string processId);
    }
}