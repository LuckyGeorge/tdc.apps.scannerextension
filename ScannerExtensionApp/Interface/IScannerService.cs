﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScannerExtensionApp.Interface
{
    public interface IScannerService
    {
        bool Initialize();
        Action<string, string> ScanAndEventSuccess { get; set; }
        Action<string> ScanNeedsConfirm { get; set; }
        Action<string> Error { get; set; }
        bool IsReady { get; }
        Action<string> Init { get; set; }
        void Exit();
    }
}