﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CommandLine;
using GalaSoft.MvvmLight.Ioc;
using NLog;
using ScannerExtensionApp.Interface;
using ScannerExtensionApp.Service;

namespace ScannerExtensionApp
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        public class Options
        {
            [Option('p', "processes", Required = false, HelpText = "List of ProccessIds", Separator = ',')]
            public IEnumerable<string> Processes { get; set; }

            [Option('l', "language", Required = false, HelpText = "The language of the ui (en or de)", Default = "en")]
            public string Language { get; set; }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("Application staring!");
            ///////////////////
            //Register Services
            ///////////////////
            SimpleIoc.Default.Register<IPersistenceService, PersistenceService>();
            SimpleIoc.Default.Register<IEventService, EventService>();
            SimpleIoc.Default.Register<IScannerService, ScannerService>();

            ////////////////////////////////////////////////////
            //Resolve PersistenceService and Set ProductId Cache
            ////////////////////////////////////////////////////
            var persistenceService = SimpleIoc.Default.GetInstance<IPersistenceService>();

            ////////////////////////////////////////////////
            //Parse Commandline Arguments and set processIds
            ////////////////////////////////////////////////
            Parser.Default.ParseArguments<Options>(e.Args).WithParsed<Options>(o =>
            {
                if (o.Processes != null && o.Processes.Any())
                {
                    var processIds = new List<string>(o.Processes);
                    persistenceService.SetProcessIds(processIds);
                }

                if (!String.IsNullOrEmpty(o.Language))
                {
                    if (o.Language.ToLowerInvariant().Equals("de"))
                        SelectCulture("de-DE");
                    else if (o.Language.ToLowerInvariant().Equals("en"))
                        SelectCulture("en-US");
                    else
                        SelectCulture("en-US");
                }
                else
                    SelectCulture("en-US");
            });

            var window = new MainWindow();
            window.Show();

            base.OnStartup(e);
        }

        public static void SelectCulture(string culture)
        {
            if (String.IsNullOrEmpty(culture))
                return;

            var dictionaryList = Application.Current.Resources.MergedDictionaries.ToList();

            //Search for the specified culture.     
            string requestedCulture = $"UIStrings.{culture}.xaml".ToLowerInvariant();
            var resourceDictionary = dictionaryList.
                FirstOrDefault(d => d.Source.OriginalString.ToLowerInvariant().EndsWith(requestedCulture));

            if (resourceDictionary == null)
            {
                //If not found, select our default language.             
                requestedCulture = "Resources//UIStrings.xaml".ToLowerInvariant();
                resourceDictionary = dictionaryList.
                    FirstOrDefault(d => d.Source.OriginalString.ToLowerInvariant().EndsWith(requestedCulture));
            }

            //If we have the requested resource, remove it from the list and place at the end.     
            //Then this language will be our string table to use.      
            if (resourceDictionary != null)
            {
                Application.Current.Resources.MergedDictionaries.Remove(resourceDictionary);
                Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
            }

            //Inform the threads of the new culture.     
            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

        }

        protected override void OnExit(ExitEventArgs e)
        {
            var scannerService = SimpleIoc.Default.GetInstance<IScannerService>();
            scannerService.Exit();
            base.OnExit(e);
        }
    }
}
