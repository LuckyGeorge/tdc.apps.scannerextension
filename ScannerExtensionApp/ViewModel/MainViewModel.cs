﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using ScannerExtensionApp.Dto;
using ScannerExtensionApp.Interface;

namespace ScannerExtensionApp.ViewModel
{
    public enum MainWindowState
    {
        ShowInit,
        ShowWaitingForScan,
        ShowError,
        ShowProcessIdSelect,
        ShowConfirm,
    }

    public class MainViewModel : ViewModelBase
    {
        #region Microservices
        private readonly IScannerService _scannerService;
        private readonly IPersistenceService _persistenceService;
        private readonly IEventService _eventService;

        #endregion

        #region Fields
        private MainWindowState _state;
        private string _processId;
        private ScanToProcessDto _selectedProcess;
        private string _scannedCode;
        private string _error;
        #endregion

        #region Command
        private RelayCommand _confirmNewCommand;
        private RelayCommand _confirmCommand;
        private RelayCommand _changeCommand;
        private RelayCommand _abortCommand;
        

        public RelayCommand ConfirmNewCommand => _confirmNewCommand ?? (_confirmNewCommand =
                                              new RelayCommand(OnConfirmNewCommand,
                                                  CanConfirmCommand));

        public RelayCommand ConfirmCommand => _confirmCommand ?? (_confirmCommand =
            new RelayCommand(OnConfirmCommand,
                CanConfirmCommand));

        public RelayCommand ChangeCommand => _changeCommand ?? (_changeCommand =
            new RelayCommand(OnChangeCommand,
                CanChangeCommand));

        public RelayCommand AbortCommand => _abortCommand ?? (_abortCommand =
            new RelayCommand(OnAbortCommand, () => true));

        private bool CanChangeCommand()
        {
            return !String.IsNullOrEmpty(ProcessId);
        }

        private bool CanConfirmCommand()
        {
            return !String.IsNullOrEmpty(ProcessId);
        }

        private void OnChangeCommand()
        {
            Task.Run(async () =>
            {
                Application.Current.Dispatcher.Invoke(() => ProcessIds.Clear());
                foreach (var process in await _persistenceService.GetProcesses())
                {
                    Application.Current.Dispatcher.Invoke(() => ProcessIds.Add(process));
                }
                Application.Current.Dispatcher.Invoke(() => State = MainWindowState.ShowProcessIdSelect);
            });
        }

        private void OnAbortCommand()
        {
            ScannedCode = String.Empty;
            SelectedProcess = null;
            State = MainWindowState.ShowWaitingForScan;
        }

        private void OnConfirmCommand()
        {
            Task.Run(async () =>
            {
                await _eventService.SendProcessEvent(ProcessId);
            });
            Application.Current.Shutdown();
        }

        private void OnConfirmNewCommand()
        {
            Task.Run(async () =>
            {
                if(!String.IsNullOrEmpty(SelectedProcess.ScanId))
                    await _persistenceService.ChangeProcess(SelectedProcess.ScanId, ScannedCode);
                else
                    await _persistenceService.AddProcess(ScannedCode, ProcessId);
                await _eventService.SendProcessEvent(ProcessId);
            });
            Application.Current.Shutdown();
        }



        #endregion


        #region Properties
        public MainWindowState State
        {
            get => _state;
            set => Set(ref _state, value);
        }

        public string Error
        {
            get => _error;
            set => Set(ref _error, value);
        }

        public string ScannedCode
        {
            get => _scannedCode;
            set => Set(ref _scannedCode, value);
        }

        public string ProcessId
        {
            get => _processId;
            set
            {
                Set(ref _processId, value);
                ((RelayCommand)ConfirmNewCommand).RaiseCanExecuteChanged();
                ((RelayCommand)ConfirmCommand).RaiseCanExecuteChanged();
                ((RelayCommand)ChangeCommand).RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<ScanToProcessDto> ProcessIds { get; set; } = new ObservableCollection<ScanToProcessDto>();

        public ScanToProcessDto SelectedProcess
        {
            get => _selectedProcess;
            set
            {
                Set(ref _selectedProcess, value);
                ProcessId = _selectedProcess != null ? _selectedProcess.ProcessId : String.Empty;
            }
        }

        #endregion

        public MainViewModel(IScannerService scannerService,
                             IPersistenceService persistenceService,
                             IEventService eventService)
        {
            _scannerService = scannerService;
            _persistenceService = persistenceService;
            _eventService = eventService;

            //Register Callbacks
            _scannerService.ScanAndEventSuccess += OnScanAndEventSuccess;
            _scannerService.ScanNeedsConfirm += OnScanNeedsConfirm;
            _scannerService.Error += OnError;

            //Set Initial Window State
            State = MainWindowState.ShowInit;

            //Initialize Services
            Task.Run(() =>
            {
                var init = _scannerService.Initialize();
                if (init)
                    Application.Current.Dispatcher.Invoke(() => State = MainWindowState.ShowWaitingForScan);
                else
                    Application.Current.Dispatcher.Invoke(() => State = MainWindowState.ShowError);
            });
        }

        private void OnError(string errorString)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Error = errorString;
                State = MainWindowState.ShowError;
            });
        }

        private void OnScanNeedsConfirm(string scanId)
        {
            Application.Current.Dispatcher.Invoke(() => ScannedCode = scanId);
            Application.Current.Dispatcher.Invoke(() => ProcessIds.Clear());
            Task.Run(async () =>
            {
                foreach (var process in await _persistenceService.GetProcesses())
                {
                    Application.Current.Dispatcher.Invoke(() => ProcessIds.Add(process));
                }
            });
            Application.Current.Dispatcher.Invoke(() => State = MainWindowState.ShowProcessIdSelect);
        }

        private void OnScanAndEventSuccess(string scanId, string processId)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                ScannedCode = scanId;
                ProcessId = processId;
                State = MainWindowState.ShowConfirm;
            });
        }
    }
}