﻿namespace ScannerExtensionApp.Dto
{
    public class ScanToProcessDto
    {
        public string ScanId { get; set; }
        public string ProcessId { get; set; }
    }
}