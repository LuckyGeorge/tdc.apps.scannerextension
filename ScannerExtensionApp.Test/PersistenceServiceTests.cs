using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using ScannerExtensionApp.Interface;
using ScannerExtensionApp.Service;

namespace ScannerExtensionApp.Test
{
    public class PersistenceServiceTests
    {
        private IPersistenceService _persistenceService;
        [SetUp]
        public void Setup()
        {
            _persistenceService = new PersistenceService();
        }

        [Test]
        public async Task Test_Initialize()
        {
            await _persistenceService.AddProcess("12345", "process1");
            await _persistenceService.AddProcess("12346", "process2");
            await _persistenceService.AddProcess("12347", "process3");
            await _persistenceService.Initialize();

            var processes = await _persistenceService.GetProcesses();
            processes.Count.Should().Be(3);

            await _persistenceService.Initialize(true);

            processes = await _persistenceService.GetProcesses();
            processes.Count.Should().Be(0);
        }

        [Test]
        public async Task Test_Add_Get()
        {
            await _persistenceService.Initialize(true);
            await _persistenceService.AddProcess("12345", "process1");
            await _persistenceService.AddProcess("12346", "process2");
            await _persistenceService.AddProcess("12347", "process3");

            var processes = await _persistenceService.GetProcesses();
            processes.Count.Should().Be(3);

            var result = await _persistenceService.GetProcess("12345");
            result.Should().Be("process1");
            result = await _persistenceService.GetProcess("12347");
            result.Should().Be("process3");
            result = await _persistenceService.GetProcess("00000");
            result.Should().BeEmpty();
        }

        [Test]
        public async Task Test_Remove_Get()
        {
            await _persistenceService.Initialize(true);
            await _persistenceService.AddProcess("12345", "process1");
            await _persistenceService.AddProcess("12346", "process2");
            await _persistenceService.AddProcess("12347", "process3");

            var processes = await _persistenceService.GetProcesses();
            processes.Count.Should().Be(3);

            var result = await _persistenceService.RemoveProcess("12345");
            result.Should().BeTrue();

            processes = await _persistenceService.GetProcesses();
            processes.Count.Should().Be(2);

            result = await _persistenceService.RemoveProcess("00000");
            result.Should().BeFalse();

            processes = await _persistenceService.GetProcesses();
            processes.Count.Should().Be(2);
        }
    }
}