using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using ScannerExtensionApp.Interface;
using ScannerExtensionApp.Service;

namespace ScannerExtensionApp.Test
{
    public class EventServiceTests
    {
        private IEventService _eventService;
        private NamedPipeServerStream _namedPipeServer;
        [SetUp]
        public void Setup()
        {
            _eventService = new EventService();

            
        }

        [Test]
        public async Task Test_SendEvent_Internal()
        {
            var processIdToSignal = "1234567890";
            var waiter = new AutoResetEvent(false);

            //Setup NamedPipeServer
            _namedPipeServer = new NamedPipeServerStream(EventService.PipeName, PipeDirection.In); //Don't forget to dispose !!!

            //Starting Task to wait for connection
            _ = Task.Run(async () =>
              {
                  await _namedPipeServer.WaitForConnectionAsync();

                  using (StreamReader sr = new StreamReader(_namedPipeServer))
                  {
                      var serverProcessOutput = await sr.ReadToEndAsync();
                      serverProcessOutput.Should().NotBeNullOrEmpty();
                      serverProcessOutput.Should().Be(processIdToSignal);
                  }

                  await _namedPipeServer.DisposeAsync();
                  waiter.Set();
              });

            Thread.Sleep(100); //Ok, this is the lazy way to wait for the named pipe server to spin up

            var result = await _eventService.SendProcessEvent(processIdToSignal);
            result.Should().BeTrue();

            //Wait for the server to complete
            var timeOut = waiter.WaitOne(3000);
            timeOut.Should().BeTrue();
        }

        [Test]
        public async Task Test_SendEvent_External()
        {
            var processIdToSignal = "0987654321";
            var waiter = new AutoResetEvent(false);

            string solutionDir = Path.GetDirectoryName(Path.GetDirectoryName(TestContext.CurrentContext.TestDirectory));
            Debug.Assert(solutionDir != null, nameof(solutionDir) + " != null");
            string serverExePath = Path.Combine(solutionDir, "..\\..", "ScannerExtensionPipeServer\\Debug", "ScannerExtensionPipeServer.exe");

            //Start named pipe server from as external cpp console application and capture standard output
            var processStartInfo = new ProcessStartInfo
            {
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                UseShellExecute = false,
                FileName = serverExePath
            };

            var serverProcess = new Process { StartInfo = processStartInfo, EnableRaisingEvents = true };
            var serverOutput = new StringBuilder();

            serverProcess.Start();
            serverProcess.BeginOutputReadLine();
            serverProcess.OutputDataReceived += (sender, eventArgs) =>
            {
                serverOutput.AppendLine(eventArgs.Data);
                waiter.Set();
            };

            Thread.Sleep(500); //Wait for the server to spin up

            var result = await _eventService.SendProcessEvent(processIdToSignal);
            result.Should().BeTrue();

            var noTimeOut = waiter.WaitOne(10000);
            noTimeOut.Should().BeTrue();

            if(!serverProcess.HasExited)
                serverProcess.Kill();
            serverOutput.ToString().Should().Contain(processIdToSignal);
        }
    }
}