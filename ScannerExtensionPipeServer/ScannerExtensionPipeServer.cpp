#include <windows.h> 
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>
#include <iostream>
#include <string>

/// <summary>
/// Helper method for cout
/// </summary>
/// <param name="buffer"></param>
/// <param name="bufflen"></param>
/// <returns></returns>
std::string bufferToString(char* buffer, int bufflen)
{
    std::string ret(buffer, bufflen);
    return ret;
}

/// <summary>
/// This application creates a simplte named pipe server in c++ and waits for a client to connect.
/// After the client connects the server returns the string send from the client as return code
/// </summary>
/// <returns></returns>
int main()
{
    HANDLE hPipe;
    char buffer[1024];
    DWORD dwRead;

    hPipe = CreateNamedPipe(TEXT("\\\\.\\pipe\\ScannerExtensionPipe"),
        PIPE_ACCESS_INBOUND,
        PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
        1,
        1024 * 16,
        1024 * 16,
        NMPWAIT_USE_DEFAULT_WAIT,
        NULL);

    while (hPipe != INVALID_HANDLE_VALUE)
    {
        if (ConnectNamedPipe(hPipe, NULL) != FALSE)   // wait for someone to connect to the pipe
        {
            while (ReadFile(hPipe, buffer, sizeof(buffer) - 1, &dwRead, NULL) != FALSE)
            {
                /* add terminating zero */
                buffer[dwRead] = '\0';

                /* write buffer to std::out - so we can capture it during the unit test */
                std::cout << bufferToString(buffer, dwRead) << "\n";
                std::cout.flush();
            }
        }

        DisconnectNamedPipe(hPipe);
    }

    return 0;
}


